<div align="center">
  <img src="./docs/img/jwt_django.png" alt="Train Icon" width="300">
</div>

<div align="center">

# Simplest implementation of JWT Django
[![jwt](https://img.shields.io/badge/JWT-Auth0-purple.svg)](https://jwt.io/)
[![Django](https://img.shields.io/badge/Django-4.0.5-darkgreen.svg)](https://www.djangoproject.com/)
[![djangorestframework](https://img.shields.io/badge/django%20rest%20framework-3.13.1-darkgreen.svg)](https://www.django-rest-framework.org/)
[![djangorestframeworkjwt](https://img.shields.io/badge/django%20rest%20framework%20jwt-1.11.0-darkgreen.svg)](https://pypi.org/project/djangorestframework-jwt/)
<img src="https://img.shields.io/badge/Vanilla-JavaScript-yellow.svg" alt="Vanilla JS">

</div>

<div align="center">
  Implementación de JWT con Django.
</div>

## ¿En qué consiste la implentación de JWT?
La implentación de JWT consiste en usar un token que debe ir renovándose cada cierto tiempo con una token_refresh.

## Instalación

```
pip install -r requirements.txt
python manage.py runserver
```

## Capturas de pantalla
<p align="center">
  <img src="./docs/img/mockup_jwt_login.png" alt="Login">
</p>

### Ingreso de credenciales de acceso
<div align="center">
  <img src="./docs/img/ingreso_credenciales.png" alt="Credenciales" width="500">
</div>

### Llaves obtenidas al ingresar
<div align="center">
  <img src="./docs/img/llaves_obtenidas.png" alt="Llaves obtenidas" width="500">
</div>

### Botones para intentar acceder a contenido protegido

_Contenido protegido_ se refiere a vistas que sólo pueden ser accedidas mediante un usuario con un token válido.

<div align="center">
  <img src="./docs/img/contenido_protegido.png" alt="Contenido Protegido" width="500">
</div>

### Acceso permitido al contenido protegido
<div align="center">
  <img src="./docs/img/acceso_permitido.png" alt="Acceso Permitido" width="500">
</div>

### Acceso denegado al contenido protegido
<div align="center">
  <img src="./docs/img/acceso_denegado.png" alt="Acceso denegado" width="500">
</div>

## References.
- [Curl Common Options](https://gist.github.com/subfuzion/08c5d85437d5d4f00e58)
- [Django Rest Framework y JWT para autenticar usuarios](https://coffeebytes.dev/django-rest-framework-y-jwt-para-autenticar-usuarios/)
- [JS Fetch](https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Using_Fetch)
- [Template form 20](https://preview.colorlib.com/theme/bootstrap/login-form-20/)
- [Mockup](https://screenshot.rocks/app#)