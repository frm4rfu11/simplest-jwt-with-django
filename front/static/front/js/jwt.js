var requestedToken = "000.111.222";
var requestedRefresh = "333.222.111";

function requestToken() {
  separador = "  -  ";
  user = document.getElementById("username-field").value;
  password = document.getElementById("password-field").value;
  token = document.getElementById("token");
  refresh = document.getElementById("refresh");

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    username: user,
    password: password,
  });

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  fetch("http://127.0.0.1:8000/api/token/", requestOptions)
    .then((response) => response.json())
    .then((result) => {
      if (result.access === undefined) {
        validarCredenciales();
        return false;
      }
      requestedToken = result.access;
      theToken = result.access.split(".");
      token.innerText = "Token " + theToken[2].substring(0, 15);
      //requestedToken[0].substring(0,5)
      //+ separador + requestedToken[1].substring(0,5)
      //+ separador + requestedToken[2].substring(0,5);

      requestedRefresh = result.refresh;
      theRefresh = result.refresh.split(".");
      refresh.innerText = "Refresh " + theRefresh[2].substring(0, 15);
      //requestedRefresh[0].substring(0,5)
      //+ separador + requestedRefresh[1].substring(0,5)
      //+ separador + requestedRefresh[2].substring(0,5);
    })
    .catch((error) => console.log("error", error));
}

function accederAlContenidoProtegido() {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "text/plain");
  myHeaders.append("Authorization", "Bearer " + requestedToken);

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  fetch("http://127.0.0.1:8000/protegida/", requestOptions)
    .then((response) => response.json())
    .then((result) => {
      if (result.detail) {
        document.getElementById("btnContenidoProtegido").innerText =
          result.code;
      } else {
        document.getElementById("btnContenidoProtegido").innerText =
          result.content;
      }
    })
    .catch((error) => {
      document.getElementById("btnContenidoProtegido").innerText = error;
    });
}

function renovarToken() {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    refresh: requestedRefresh,
  });

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  let textoBtnRenovar = document.getElementById("btnRenovarToken").innerText;
  document.getElementById("btnRenovarToken").innerText = "...";
  setTimeout(function () {
    document.getElementById("btnRenovarToken").innerText = textoBtnRenovar;
  }, 1000);

  fetch("http://127.0.0.1:8000/api/token/refresh/", requestOptions)
    .then((response) => response.json())
    .then((result) => {
      requestedToken = result.access;
      theToken = result.access.split(".");
      token.innerText = "Token " + theToken[2].substring(0, 15);
    })
    .catch((error) => {
      document.getElementById("btnRenovarToken").innerText = "¡¡¡ERROR!!!";
      setTimeout(function () {
        document.getElementById("btnRenovarToken").innerText = textoBtnRenovar;
      }, 1000);
    });
}

function validarCredenciales() {
  var element = document.getElementById("invalid");
  element.classList.toggle("d-none");
}
